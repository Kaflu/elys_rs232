﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Threading;

namespace Elys_RS {
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window {

        public MainWindow mainWindow;
        static SerialPort serialPort;
        Thread readThread = new Thread(dataRead);
        bool cont = true;

        public MainWindow() {
            InitializeComponent();
            checkExistingPorts();
        }

        private void checkExistingPorts() {
            byte portsCounter = 0;

            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports) {
                serialPortList.Items.Add(port);
                if (portsCounter == 0) {
                    serialPortList.SelectedItem = serialPortList.Items.GetItemAt(portsCounter);
                }
                portsCounter++;
            }
        }

        private Parity getParity() {
            Parity parity = Parity.None;

            if (noneRB.IsChecked == true) {
                parity = Parity.None;
            } else if (evenRB.IsChecked == true) {
                parity = Parity.Even;
            } else if (oddRB.IsChecked == true) {
                parity = Parity.Odd;
            }

            return parity;
        }

        private int getDataBits() {
            int dataBits = 0;

            if (sevenSignsRB.IsChecked == true) {
                dataBits = 7;
            } else if (eightSignsRB.IsChecked == true) {
                dataBits = 8;
            }

            return dataBits;
        }

        private StopBits getStopBits() {
            StopBits stopBits = StopBits.One;

            if (oneStopBitRB.IsChecked == true) {
                stopBits = StopBits.One;
            }
            else if (twoStopBitsRB.IsChecked == true) {
                stopBits = StopBits.Two;
            }

            return stopBits;

        }
        
        private Handshake getHandshake() {
            Handshake handshake = Handshake.None;

            if(noneControlRB.IsChecked == true) {
                handshake = Handshake.None;
            } else if(handshakeRB.IsChecked == true) {
                handshake = Handshake.RequestToSend;
            } else if (xonXoffRB.IsChecked == true) {
                handshake = Handshake.XOnXOff;
            }

            return handshake;
        }

        private string getTerminator() {
            char terminatorPart;
            string terminator = "";

            if(noTerminatorRB.IsChecked == true) {
                terminator = "";
            } else if(crTerminatorRB.IsChecked == true) {
                terminatorPart = (char)13;
                terminator = terminatorPart.ToString();
            } else if(lfTerminatorRB.IsChecked == true) {
                terminatorPart = (char)10;
                terminator = terminatorPart.ToString();
            } else if(crLfTerminatorRB.IsChecked == true) {
                terminatorPart = (char)13;
                terminator = terminatorPart.ToString();
                terminatorPart = (char)10;
                terminator = terminator + terminatorPart.ToString();
            } else if(customizeTerminatorRB.IsChecked == true) {
                terminator = customizeTerminatorTB.Text.ToString();
            }

            return terminator;
        }

        private void connectionButton_Click(object sender, RoutedEventArgs e) {

            String portName = serialPortList.Text;
            int baudRate = Int32.Parse(baudRateList.Text);
            Parity parity = getParity();
            int dataBits = getDataBits();
            StopBits stopBits = getStopBits();

            serialPort = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
            serialPort.Handshake = getHandshake();

            serialPort.ReadTimeout = 500;
            serialPort.WriteTimeout = 500;

            serialPort.Open();
        }


        private void dataSend()
        {
            
            string message = "";

            while(cont)
            {
                message = textSend.Text;

                if (message.Equals("quit"))
                    cont = false;
                else
                    serialPort.WriteLine(message);
            }


            readThread.Join();
            serialPort.Close();
        }


        public static void dataRead()
        {
            bool cont = true;

            while(cont)
            {
                try
                {
                    string message = serialPort.ReadLine();
                    //textRcvd.Text = message;
                } catch(TimeoutException)
                { }
            }
        }

        private void customizeTerminatorRB_Checked(object sender, RoutedEventArgs e) {
            customizeTerminatorTB.IsEnabled = true;
            customizeTerminatorTB.Background = Brushes.White;
        }

        private void defaultTerminator_Checked(object sender, RoutedEventArgs e) {
            customizeTerminatorTB.IsEnabled = false;
            customizeTerminatorTB.Background = Brushes.Gray;
            customizeTerminatorTB.Text = "";
        }

        private void sendButton_Click(object sender, RoutedEventArgs e) {

            dataSend();
        }
    }
}
